package main

import (
	"errors"
	"fmt"
	"log/syslog"
	"math/rand"
	"os"
	"os/exec"
	"time"

	"github.com/sirupsen/logrus"
	logrus_syslog "github.com/sirupsen/logrus/hooks/syslog"
)

type worker struct {
	onIterBegin func(int)
	onIterEnd   func(int, time.Duration)
}

type jobResult struct {
	j   int
	err error
}

type jobs struct {
	interval, offset, recheckEach time.Duration
	onExec                        func(int, time.Duration, chan jobResult, *int)
	onWaiting                     func(int)
	onFinish                      func(int, error)
}

type maintenance struct {
	windowEnd, recheckEach time.Duration
	ended                  func() bool
	onWaiting              func()
}

type machine struct {
	warmup, minShutdown time.Duration
	setWakeAndShutdown  func(time.Duration) bool
	onSkipShutdown      func()
}

func now() time.Duration {
	return time.Duration(time.Now().UnixNano())
}

func date(t time.Duration) time.Time {
	return time.Unix(int64(t/time.Second), int64(t%time.Second))
}

func execTime(relativeTo, interval, offset time.Duration) time.Duration {
	next := offset - relativeTo%interval
	if next < 0 {
		next += interval
	}
	return relativeTo + next
}

func scheduleWake(relativeTo, interval, offset, warmup, minShutdown time.Duration, schedule func(time.Duration) bool) bool {
	if next := execTime(relativeTo, interval, offset) - warmup; next > relativeTo+minShutdown {
		return schedule(next)
	}
	return false
}

func mainLoopSleep(relativeTo, nextExec, execEnd, maintEnd time.Duration) time.Duration {
	if maintEnd > relativeTo && maintEnd < nextExec {
		return maintEnd - relativeTo
	}
	if execEnd > relativeTo && execEnd < nextExec {
		return execEnd - relativeTo
	}
	return nextExec - relativeTo
}

func mainLoop(worker worker, jobs jobs, maintenance maintenance, machine machine) bool {
	i, j, n := 0, 0, 0
	var sleep time.Duration
	finished := make(chan jobResult)

	nextExec := execTime(now(), jobs.interval, jobs.offset)
	execEnd := nextExec
	maintEnd := maintenance.windowEnd
	for {
		i++
		worker.onIterBegin(i)

		if nextExec <= now() {
			execEnd = nextExec
			j++
			n++
			go jobs.onExec(j, nextExec, finished, &n)
			nextExec = execTime(now(), jobs.interval, jobs.offset)
			continue
		}

		if execEnd < now() {
			select {
			case jobResult := <-finished:
				jobs.onFinish(jobResult.j, jobResult.err)
			default:
				jobs.onWaiting(n)
			}
			execEnd = now()
			if n > 0 {
				execEnd += jobs.recheckEach
			}
		}

		if maintEnd <= now() && n == 0 {
			if maintenance.ended() {
				if scheduleWake(now(), jobs.interval, jobs.offset, machine.warmup, machine.minShutdown, machine.setWakeAndShutdown) {
					return true
				}
				machine.onSkipShutdown()
			} else {
				maintenance.onWaiting()
				maintEnd = now() + maintenance.recheckEach
			}
		}

		sleep = mainLoopSleep(now(), nextExec, execEnd, maintEnd)
		worker.onIterEnd(i, sleep)
		time.Sleep(sleep)
	}
}

func testJob() (err error) {
	b := now()
	u, reset := "\033[4m", "\033[0m"
	out := func(stl string, str string) { fmt.Print(stl + str + reset) }
	rand.Seed(time.Now().UnixNano())
	max := rand.Intn(80) + 1
	for i := 0; i < max; i++ {
		switch i % 4 {
		case 0:
			out(u, "|")
		case 1:
			out(u, "/")
		case 2:
			out(u, "-")
		case 3:
			out(u, "\\")
		}
		time.Sleep(time.Duration(100) * time.Millisecond)
		fmt.Print("\b")
	}
	out(u, fmt.Sprint(max, " @ ", now()-b, "\n"))
	if max%2 == 1 {
		err = errors.New("this is odd")
	}
	return
}

func users() (users int, err error) {
	out, err := exec.Command("who").Output()
	if err == nil {
		for _, outByte := range out {
			if outByte == 0x0a {
				users++
			}
		}
	}
	rand.Seed(time.Now().UnixNano())
	if rand.Int31()%10 == 0 {
		users++
	}
	return
}

func writeRTCWake(wake time.Duration) bool {
	rand.Seed(time.Now().UnixNano())
	if rand.Int31()%3 == 0 {
		log.Warn("SETTING RTC wake FAILED")
		return false
	}
	log.WithFields(logrus.Fields{"timestamp": int(wake / time.Second), "datetime": date(wake)}).Info("RTC wake SET")
	return true
}

func shutdown() bool {
	rand.Seed(time.Now().UnixNano())
	if rand.Int31()%3 == 0 {
		log.Warn("SHUTDOWN FAILED")
		return false
	}
	log.Info("SHUTDOWN ", func(h string, e error) string {
		if e != nil {
			return "me"
		}
		return h
	}(os.Hostname()), " now")
	return true
}

var log = logrus.New()

func init() {
	hook, err := logrus_syslog.NewSyslogHook("", "", syslog.LOG_INFO, "")
	if err == nil {
		log.Hooks.Add(hook)
	}

	log.SetFormatter(&logrus.TextFormatter{DisableColors: true, FullTimestamp: true})
	log.SetOutput(os.Stdout)
	log.SetLevel(logrus.TraceLevel)
}

func main() {
	log.Debug("CRONOBOT BEGIN")

	rand.Seed(time.Now().UnixNano())
	if rand.Int31()%2 == 0 {
		log.Fatal("FATAL ERROR - exiting...")
		return
	}

	worker := worker{
		func(i int) { log.Debug("ITERATION # ", i) },
		func(i int, sleep time.Duration) { log.WithField("sleep", sleep).Debug("SLEEP") },
	}

	jobs := jobs{
		30 * time.Second, 5 * time.Second, 1 * time.Second,
		func(j int, next time.Duration, finished chan jobResult, pn *int) {
			defer func(pn *int) { *pn-- }(pn)

			log.WithField("planned", date(next)).Info("JOB #", j, " BEGIN")

			err := testJob()

			log.Info("JOB # ", j, " END")
			finished <- jobResult{j, err}
		},
		func(n int) {
			log.Info("WAITING for ", n, " job(s) to be finished")
		},
		func(j int, err error) {
			if err == nil {
				log.Info("JOB # ", j, " successful FINISH detected")
			} else {
				log.WithField("error", err).Warn("JOB # ", j, " FINISH detected, finished with error")
			}
		},
	}

	usersBefore, _ := users()
	maintenance := maintenance{
		now() + 10*time.Second, 1 * time.Second,
		func() bool {
			usersNow, _ := users()
			return usersNow <= usersBefore
		},
		func() { log.Info("WAITING for users to logout") },
	}

	machine := machine{
		5 * time.Second, 15 * time.Second,
		func(wake time.Duration) bool {
			if !writeRTCWake(wake) {
				log.Error("NOT SETTING wake - setting wake HAVE FAILED")
				return false
			}
			if !shutdown() {
				log.Error("NOT SHUTTING DOWN - shutdown HAVE FAILED")
				return false
			}
			return true
		},
		func() {
			log.Info("WAITING - the next wake is earlier then minimum shutdown OR setting wake or shutdown have failed")
		},
	}

	if mainLoop(worker, jobs, maintenance, machine) {
		log.Debug("CRONOBOT END")
	}
}
