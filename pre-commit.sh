#!/usr/bin/env bash
gofmt -s -l ./*.go && go vet && go test -cover
