#!/usr/bin/env bash
if [[ "$EUID" -eq "0" ]]; then
    # Do not run as superuser
    >&2 echo "Error: Do not run as superuser, please run as a regular user"
    exit 1
else
    # Install pre-commit hook if it's not present or doesn't point to the right location
    if [[ ! -L ".git/hooks/pre-commit" ]] || [ "$(readlink .git/hooks/pre-commit)" != "../../pre-commit.sh" ]; then
        ln -s ../../pre-commit.sh .git/hooks/pre-commit
        if [ "$?" ]; then
            echo "Success: Pre-commit hook have been installed"
        else
            echo "Warning: Pre-commit hook installation failed"
        fi
    else
        echo "OK: Pre-commit hook is installed"
    fi
    
    # Install pre-push hook if it's not present or doesn't point to the right location
    if [[ ! -L ".git/hooks/pre-push" ]] || [ "$(readlink .git/hooks/pre-push)" != "../../pre-commit.sh" ]; then
        ln -s ../../pre-commit.sh .git/hooks/pre-push
        if [ "$?" ]; then
            echo "Success: Pre-push hook have been installed"
        else
            echo "Warning: Pre-push hook installation failed"
        fi
    else
        echo "OK: Pre-push hook is installed"
    fi
fi
