package main

import (
	"fmt"
	"testing"
	"time"
)

func TestNow(t *testing.T) {
	now := now()
	if now < 1577836800*time.Second {
		t.Errorf("Current timestamp shouldn't be from the past, got %v", now)
	}
	if now > 4102444800*time.Second {
		t.Errorf("Current timestamp shouldn't be from the future, got %v", now)
	}
}

func TestDate(t *testing.T) {
	now := time.Now().Round(0)
	result := date(time.Duration(now.UnixNano()))
	if now != result {
		t.Errorf("Got %v, wanted %v", result, now)
	}
}

func TestExecTime(t *testing.T) {
	var testData = []struct {
		relativeTo, interval, offset, expected time.Duration
	}{
		{0 * time.Second, 3600 * time.Second, 0 * time.Second, 0 * time.Second},
		{3600 * time.Second, 3600 * time.Second, 0 * time.Second, 3600 * time.Second},
		{3601 * time.Second, 3600 * time.Second, 0 * time.Second, 7200 * time.Second},
		{0 * time.Second, 3600 * time.Second, 900 * time.Second, 900 * time.Second},
		{4499 * time.Second, 3600 * time.Second, 900 * time.Second, 4500 * time.Second},
		{4500 * time.Second, 3600 * time.Second, 900 * time.Second, 4500 * time.Second},
		{4501 * time.Second, 3600 * time.Second, 900 * time.Second, 8100 * time.Second},
	}

	for _, td := range testData {
		testname := fmt.Sprintf("%v,%v,%v", td.relativeTo, td.interval, td.offset)
		t.Run(testname, func(t *testing.T) {
			result := execTime(td.relativeTo, td.interval, td.offset)
			if result != td.expected {
				t.Errorf("Got %v, wanted %v, inputs %v, %v, %v", result, td.expected, td.relativeTo, td.interval, td.offset)
			}
		})
	}
}

func TestScheduleWake(t *testing.T) {
	var testData = []struct {
		relativeTo, interval, offset, warmup, minShutdown time.Duration
		expected                                          bool
	}{
		{0 * time.Second, 3600 * time.Second, 0 * time.Second, 900 * time.Second, 1200 * time.Second, false},
		{1 * time.Second, 3600 * time.Second, 0 * time.Second, 900 * time.Second, 1200 * time.Second, true},
		{2400 * time.Second, 3600 * time.Second, 0 * time.Second, 900 * time.Second, 1200 * time.Second, false},
		{2401 * time.Second, 3600 * time.Second, 0 * time.Second, 900 * time.Second, 1200 * time.Second, false},
		{0 * time.Second, 3600 * time.Second, 900 * time.Second, 900 * time.Second, 1200 * time.Second, false},
		{900 * time.Second, 3600 * time.Second, 900 * time.Second, 900 * time.Second, 1200 * time.Second, false},
		{901 * time.Second, 3600 * time.Second, 900 * time.Second, 900 * time.Second, 1200 * time.Second, true},
		{2399 * time.Second, 3600 * time.Second, 900 * time.Second, 900 * time.Second, 1200 * time.Second, true},
		{2400 * time.Second, 3600 * time.Second, 900 * time.Second, 900 * time.Second, 1200 * time.Second, false},
	}

	for _, td := range testData {
		testname := fmt.Sprintf("%v,%v,%v,%v,%v", td.relativeTo, td.interval, td.offset, td.warmup, td.minShutdown)
		t.Run(testname, func(t *testing.T) {
			result := scheduleWake(td.relativeTo, td.interval, td.offset, td.warmup, td.minShutdown, func(nextWake time.Duration) bool { return true })
			if result != td.expected {
				t.Errorf("Got %v, wanted %v, inputs %v, %v, %v, %v, %v", result, td.expected, td.relativeTo, td.interval, td.offset, td.warmup, td.minShutdown)
			}
		})
	}
}

func TestMainLoopSleep(t *testing.T) {
	var testData = []struct {
		relativeTo, nextExec, execEnd, maintEnd, expected time.Duration
	}{
		{0 * time.Second, 3600 * time.Second, 3600 * time.Second, 600 * time.Second, 600 * time.Second},
		{10 * time.Second, 3600 * time.Second, 3600 * time.Second, 600 * time.Second, 590 * time.Second},
		{599 * time.Second, 3600 * time.Second, 3600 * time.Second, 600 * time.Second, 1 * time.Second},
		{600 * time.Second, 3600 * time.Second, 3600 * time.Second, 600 * time.Second, 3000 * time.Second},
		{3598 * time.Second, 3600 * time.Second, 3599 * time.Second, 600 * time.Second, 1 * time.Second},
		{3599 * time.Second, 3600 * time.Second, 3600 * time.Second, 600 * time.Second, 1 * time.Second},
		{3600 * time.Second, 3600 * time.Second, 3601 * time.Second, 600 * time.Second, 0 * time.Second},
		{3601 * time.Second, 7200 * time.Second, 3602 * time.Second, 600 * time.Second, 1 * time.Second},
		{3602 * time.Second, 7200 * time.Second, 3603 * time.Second, 600 * time.Second, 1 * time.Second},
	}

	for _, td := range testData {
		testname := fmt.Sprintf("%v,%v,%v,%v", td.relativeTo, td.nextExec, td.execEnd, td.maintEnd)
		t.Run(testname, func(t *testing.T) {
			result := mainLoopSleep(td.relativeTo, td.nextExec, td.execEnd, td.maintEnd)
			if result != td.expected {
				t.Errorf("Got %v, wanted %v, inputs %v, %v, %v, %v", result, td.expected, td.relativeTo, td.nextExec, td.execEnd, td.maintEnd)
			}
		})
	}
}

func TestUsers(t *testing.T) {
	_, err := users()
	if err != nil {
		t.Error("Counting logins failed", err)
	}
}
