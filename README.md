# Cronobot

A time-based computer wake-up and job execution scheduler. Runs a job (commands or shell scripts) at fixed intervals. Useful for recurrently scheduled jobs with plenty of idle time between executions, e.g. daily or weekly backups.

## Features

- **Executes a job recurrently** at a fixed `interval` (how often to execute) and `offset` (when exactly execute within interval)
- **Schedules machine boot up and shutdowns machine** with a specified `warm-up` period (how long wake the machine in advance)
- **Keeps machine running and ready to log in** after boot up for a specified `maintenance window` duration (how long to wait for login after boot)
- **Keeps machine running when somebody is logged in** until all users log out
- **Skips shutdown and keeps machine running** when remaining time to the next job would be too short - shorter than specified `minimal shutdown` time (the time when turning the machine off wouldn't be reliable)
- **Handles long-running and concurrent jobs properly**
- **Doesn't turn off the machine** until all jobs are finished
- **Doesn't miss a job during maintenance** when somebody is logged in
- **Keeps the machine running when planning the next wake or shutdown fails**

## System requirements

- Linux with systemd
- RTC (real-time clock) system and power off ability - _almost all common PC compatible systems_
- System `boot at a specified date and time`, `wake up from S5 (off mode)` or `power on from S5 by RTC alarm` ability

To check the RTC system is present (usually `rtc0`):

```shell
ls /sys/class/rtc
```

To test RTC wake alarm works - the computer should boot up in 60 seconds:

```shell
rtcwake -m off -s 60
```

### Recommended

- Set up the machine BIOS or UEFI to:
  - **turn the machine on after power failure**, e.g. `power failure recovery`: `always on`
  - **wake on LAN**, e.g. `wake on LAN`, `power on by LAN`, `resume by LAN`, `remote wake up` or `wake on wireless LAN`: `on`
- Set up the machine's OS clock to synchronize with `NTP` or another synchronization mechanism

## Notes

- All times are compared with Unix time UTC time zone

## License

[MIT License](LICENSE)
